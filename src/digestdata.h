/*
 * This file is part of digest plugin
 *
 * Copyright (C) 2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */
#ifndef DIGESTDATA_H
#define DIGESTDATA_H

#include <SignOn/SessionData>

namespace DigestPluginNS {

/*!
 * @class DigestData
 * Data container to hold values for authentication session.
 */
class DigestData : public SignOn::SessionData
{
public:

    /*!
     * Declare property setters and getters
     */
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, Algorithm);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, nonce);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, nonceCount);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, cNonce);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, qop);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, method);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, digestUri);
    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, Entity);

    SIGNON_SESSION_DECLARE_PROPERTY(QByteArray, Digest);
};

}  // namespace DigestPluginNS

#endif // DIGESTDATA_H
