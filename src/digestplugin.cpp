/*
 * This file is part of signon
 *
 * Copyright (C) 2010 Nokia Corporation.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include <QCryptographicHash>
#include <openssl/rand.h>
#include <SignOn/signonplugincommon.h>

#include "digestplugin.h"

using namespace SignOn;

namespace DigestPluginNS {

static QByteArray nonce()
{
    TRACE();

    unsigned char buf[32];
    QByteArray buffer;
    int ret = RAND_bytes(buf, 32);
    if (ret) {
        QByteArray buffer((const char*) buf, sizeof(buf));
        return buffer.toHex();
    }

    qCritical() << "RAND_bytes failed, fallback to qrand";
    //fallback to default qt implementation
    return QCryptographicHash::hash(QByteArray::number(qrand(), 16) + QByteArray::number(qrand(), 16),
                                      QCryptographicHash::Md5).toHex();

}

/*
    Following block is from qauthenticator.cpp
*/
/*
  Digest MD5 implementation

  Code taken from RFC 2617

  Currently we don't support the full SASL authentication mechanism (which includes cyphers)
*/

/* calculate request-digest/response-digest as per HTTP Digest spec */
static QByteArray digestMd5ResponseHelper(
    const QByteArray &alg,
    const QByteArray &userName,
    const QByteArray &realm,
    const QByteArray &password,
    const QByteArray &nonce,       /* nonce from server */
    const QByteArray &nonceCount,  /* 8 hex digits */
    const QByteArray &cNonce,      /* client nonce */
    const QByteArray &qop,         /* qop-value: "", "auth", "auth-int" */
    const QByteArray &method,      /* method from the request */
    const QByteArray &digestUri,   /* requested URL */
    const QByteArray &hEntity       /* H(entity body) if qop="auth-int" */
    )
{
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(userName);
    hash.addData(":", 1);
    hash.addData(realm);
    hash.addData(":", 1);
    hash.addData(password);
    QByteArray ha1 = hash.result();
    if (alg.toLower() == "md5-sess") {
        hash.reset();
        // RFC 2617 contains an error, it was:
        // hash.addData(ha1);
        // but according to the errata page at http://www.rfc-editor.org/errata_list.php, ID 1649, it
        // must be the following line:
        hash.addData(ha1.toHex());
        hash.addData(":", 1);
        hash.addData(nonce);
        hash.addData(":", 1);
        hash.addData(cNonce);
        ha1 = hash.result();
    };
    ha1 = ha1.toHex();

    // calculate H(A2)
    hash.reset();
    hash.addData(method);
    hash.addData(":", 1);
    hash.addData(digestUri);
    if (qop.toLower() == "auth-int") {
        hash.addData(":", 1);
        hash.addData(hEntity);
    }
    QByteArray ha2hex = hash.result().toHex();

    // calculate response
    hash.reset();
    hash.addData(ha1);
    hash.addData(":", 1);
    hash.addData(nonce);
    hash.addData(":", 1);
    if (!qop.isNull()) {
        hash.addData(nonceCount);
        hash.addData(":", 1);
        hash.addData(cNonce);
        hash.addData(":", 1);
        hash.addData(qop);
        hash.addData(":", 1);
    }
    hash.addData(ha2hex);
    return hash.result().toHex();
}


    DigestPlugin::DigestPlugin(QObject *parent)
    : AuthPluginInterface(parent)
    {
        TRACE();
    }

    DigestPlugin::~DigestPlugin()
    {
        TRACE();
   }

    QString DigestPlugin::type() const
    {
        return QLatin1String("digest");
    }

    QStringList DigestPlugin::mechanisms() const
    {
        QStringList res = QStringList(QLatin1String("digest"));

        return res;
    }

    void DigestPlugin::cancel()
    {
    }

    /*
     * Digest plugin is used for returning digest
     */
    void DigestPlugin::process(const SignOn::SessionData &inData,
                                const QString &mechanism )
    {
        TRACE();
        Q_UNUSED(mechanism);
        DigestData response;
        m_data = inData.data<DigestData>();

        if (!inData.UserName().isEmpty())
            response.setUserName(inData.UserName());

        if (!inData.Secret().isEmpty()) {
            QByteArray cNonce = nonce();
            QByteArray digest = digestMd5ResponseHelper(m_data.Algorithm(),
                                                        m_data.UserName().toLatin1(),
                                   m_data.Realm().toLatin1(),
                                   m_data.Secret().toLatin1(),
                                   m_data.nonce(), m_data.nonceCount(),
                                   cNonce, m_data.qop(), m_data.method(),
                                   m_data.digestUri(), m_data.Entity());

            qDebug() << digest;
            response.setDigest(digest);
            response.setcNonce(cNonce);
            emit result(response);
            return;
        }

        //we didn't receive password from signond, so ask from user
        SignOn::UiSessionData data;
        if (inData.UserName().isEmpty())
            data.setQueryUserName(true);
        else
            data.setUserName(inData.UserName());

        data.setQueryPassword(true);
        data.setRealm(inData.Realm());
        data.setShowRealm(!data.Realm().isEmpty());
        emit userActionRequired(data);

        return;
    }

    void DigestPlugin::userActionFinished(const SignOn::UiSessionData &data)
    {
        TRACE();
        DigestData response;
        if (data.QueryErrorCode() == QUERY_ERROR_NONE) {
            response.setUserName(data.UserName());
            if (!data.Secret().isEmpty()) {
                QByteArray cNonce = nonce();
                QByteArray digest = digestMd5ResponseHelper(m_data.Algorithm(),
                                                        m_data.UserName().toLatin1(),
                                   m_data.Realm().toLatin1(),
                                   m_data.Secret().toLatin1(),
                                   m_data.nonce(), m_data.nonceCount(),
                                   cNonce, m_data.qop(), m_data.method(),
                                   m_data.digestUri(), m_data.Entity());

                qDebug() << digest;
                response.setDigest(digest);
                response.setcNonce(cNonce);
            }
            emit result(response);
            return;
        }

        if (data.QueryErrorCode() == QUERY_ERROR_CANCELED)
            emit error(Error(Error::SessionCanceled, QLatin1String("Cancelled by user")));
        else
            emit error(Error(Error::UserInteraction,
                       QLatin1String("userActionFinished error: ")
                       + QString::number(data.QueryErrorCode())));

        return;
    }

    SIGNON_DECL_AUTH_PLUGIN(DigestPlugin)
} //namespace DigestPluginNS
