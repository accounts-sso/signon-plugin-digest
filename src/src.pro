include( ../common-project-config.pri )
include( ../common-vars.pri )

TEMPLATE = lib
TARGET = digestplugin
DESTDIR = lib/signon
QT += core
QT -= gui

CONFIG += plugin \
    build_all \
    warn_on \
    link_pkgconfig \
    signon-plugins

public_headers += digestdata.h

private_headers =digestplugin.h

HEADERS = $$public_headers \
    $$private_headers

SOURCES += digestplugin.cpp

QMAKE_CXXFLAGS += -fno-exceptions \
    -fno-rtti

PKGCONFIG += \
    signon-plugins \
    signon-plugins-common \
    openssl

QMAKE_CLEAN += libdigest.so
headers.files = $$public_headers
include( ../common-installs-config.pri )
target.path = $${INSTALL_PREFIX}/lib/signon
INSTALLS = target
headers.path = $${INSTALL_PREFIX}/include/signon-plugins
INSTALLS += headers
pkgconfig.files = signon-digestplugin.pc
pkgconfig.path = $${INSTALL_PREFIX}/lib/pkgconfig
INSTALLS += pkgconfig
