#-----------------------------------------------------------------------------
# Common variables for all projects.
#-----------------------------------------------------------------------------


#-----------------------------------------------------------------------------
# Project name (used e.g. in include file and doc install path).
# remember to update debian/* files if you changes this
#-----------------------------------------------------------------------------
PROJECT_NAME = signon-digest

#-----------------------------------------------------------------------------
# Project version
# remember to update debian/* files if you changes this
#-----------------------------------------------------------------------------
PROJECT_VERSION = 1.0

#-----------------------------------------------------------------------------
# Library version
#-----------------------------------------------------------------------------
LIBRARY_VERSION = 0.1

# End of File
