include( ../common-project-config.pri )
include( ../common-vars.pri )
TARGET = signon-digestplugin-tests
QT += core \
    network
CONFIG += qtestlib \
    link_pkgconfig \
    signon-plugins

SOURCES += digestplugintest.cpp

HEADERS += digestplugintest.h \
    $${TOP_SRC_DIR}/src/digestplugin.h \
    $${TOP_SRC_DIR}/src/digestplugin.cpp \

INCLUDEPATH += . \
        $${TOP_SRC_DIR}/src \
        /usr/include/signon-qt

PKGCONFIG += \
    libsignon-qt\
    openssl

DEFINES += SIGNON_PLUGIN_TRACE

QMAKE_CXXFLAGS += -fno-exceptions \
    -fno-rtti

target.path = /usr/bin

testsuite.path  = /usr/share/$$TARGET
testsuite.files = tests.xml

INSTALLS += target \
            testsuite

check.depends = $$TARGET
check.commands = ./$$TARGET
QMAKE_EXTRA_TARGETS += check

QMAKE_CLEAN += $$TARGET

